DESCRIPTION:
This module integrates reporting.cloud web api with drupal.

INSTALLATION:
 * Put this module in your Drupal modules directory and enable it in 
   admin/modules.
 * Go to admin/config/services/reporting-cloud and enter reporting.cloud username and password.
   Select allowed templates and entities.
   
Dependencies:
  This module depends on reporting.cloud official php wrapper which can be installed via composer.
  composer require textcontrol/txtextcontrol-reportingcloud
  Place official php wrapper under libraries/reporting.cloud.
  Result structure should be
    libraries
      reporting.cloud
        composer.json
        composer.lock
        vendor
 
   