<?php
use TxTextControl\ReportingCloud;
use TxTextControl\ReportingCloud\Exception;

/**
* Implements admin_config_form().
*/
function reportingcloud_admin_config_form($form, &$form_state) {
 //reporting_cloud_get_instance();
 $credentials = variable_get('reporting_cloud', array('username' => NULL, 'password' => NULL, 'templates' => array()));

 $library = libraries_load('reporting.cloud');
 $form['reporting_cloud'] = array(
  '#tree' => TRUE,
 );
 
 $form['reporting_cloud']['username'] = array(
   '#type' => 'textfield',
   '#title' => t('Reporting.cloud Username'),
   '#required' => TRUE,
   '#default_value' => $credentials['username'],
 );
 
 $form['reporting_cloud']['password'] = array(
   '#type' => 'password',
   '#title' => t('Reporting.cloud Password'),
   '#required' => TRUE,
   '#default_value' => NULL,
   '#required' => TRUE,
 );


 $form['entities'] = array(
   '#type' => 'fieldset',
   '#title' => t('Available Entities'),
   '#tree' => TRUE,
 );
 
 foreach (entity_get_info() as $entity_type => $entity_info) {
  $form['entities'][$entity_type] = array(
    '#type' => 'checkbox',
    '#title' => check_plain($entity_info['label']),
    '#default_value' => 0,
  );

  foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
   if (count($entity_info['bundles']) > 1 || $bundle !== $entity_type) {
    $form['entities'][$entity_type . '_' . $bundle] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($bundle_info['label']),
      '#default_value' => reporting_cloud_entity_applicable($entity_type, $bundle),
    );
   }
  }
 }
 
 $form['#validate'][] = 'reportingcloud_admin_config_form_validate';
 
 $form['actions'] = array(
   '#type' => 'actions',
 );
 
 $form['actions']['submit'] = array(
   '#type' => 'submit',
   '#value' => t('Save configuration'),
 );
 
 return $form;
}

/**
* Implements hook_form_validate().
*/
function reportingcloud_admin_config_form_validate($form, &$form_state) {
  $credentials = $form_state['values'];
  $rc_instance = reporting_cloud_get_instance($credentials['reporting_cloud']['username'], $credentials['reporting_cloud']['password']);
  if ($rc_instance) {
    //try to load from remote service template list
    try {
     $settings = $rc_instance->getAccountSettings();
    } catch (Exception $e)  {
       form_set_error('username', t('Could not validate username or password. Got exception @e', array('@e' => $e->getMessage())));
    }
  }
}

/**
* Implements hook_form_submit().
*/
function reportingcloud_admin_config_form_submit($form, &$form_state) {
  $reporting_cloud = variable_get('reporting_cloud', array());
  $reporting_cloud['username'] = $form_state['values']['reporting_cloud']['username'];
  $reporting_cloud['password'] = $form_state['values']['reporting_cloud']['password'];
  variable_set('reporting_cloud', $reporting_cloud);
  variable_set('reporting_cloud_entities', $form_state['values']['entities']);
  $rc_instance = reporting_cloud_get_instance($credentials['reporting_cloud']['username'], $credentials['reporting_cloud']['password']);
}

/**
* Implements menu_callback().
*/
function reportingcloud_entity_overview() {
 $entities_settings = variable_get('reporting_cloud_entities', array());
 $rows = array();
   
 foreach (entity_get_info() as $entity_type => $entity_info) {
   /** loop over bundles **/
   foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
     if (!empty($entities_settings[$entity_type . '_' . $bundle])) {
      $base = 'admin/config/services/reporting-cloud/settings/' . $entity_type . '/' . $bundle;
      $rows[] = array(
        $bundle_info['label'],
        'opertations' =>  l(t('Edit'),  $base . '/edit') . '&nbsp;' . l(t('Delete'), $base . '/delete'),
      );
    }
 }
}

return theme('table', array(
        'header' => array(
          t('Entity'),
          t('Operations'),
        ),
        'rows' => $rows,
        'empty' => t('No configurations available'),
       )
      );
}


/**
* Implements hook_form().
*/
function reportingcloud_entity_edit_form($form, &$form_state, $entity_type, $bundle) {
  
  
  $form_state['entity_type'] = $entity_type;
  $form_state['bundle'] = $bundle;
  //get all enabled templates.
  $select = db_select('reporting_cloud_template', 'rt')->fields('rt', array('template_id', 'name', 'metadata'));
  $select->leftJoin('reporting_cloud_template_mapping', 'rtmp', 'rtmp.template_id=rt.template_id AND rtmp.entity_type=:entity_type AND rtmp.bundle=:bundle', array(':entity_type' => $entity_type, ':bundle' => $bundle));
  $select->fields('rtmp', array('mapping', 'status', 'ovverides'));
  $result = $select->execute();
  
  $form['tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array($entity_type),
  );
  
  $form['templates'] = array(
    '#type' => 'vertical_tabs',
  );

  $available_templates = array();
  foreach ($result->fetchAll() as $template_data_mapping) {
    $base_key = "template_{$template_data_mapping->template_id}";
   
    $form[$base_key] = array(
      '#type' => 'fieldset',
      '#title' => filter_xss($template_data_mapping->name),
      '#group' => 'templates',
      '#tree' => TRUE,
    );

    $form[$base_key]['status']= array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#default_value' => $template_data_mapping->status,
      
    );
    
    $form[$base_key]['ovverides'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ovverides'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => array(
        'visible' => array(':input[name="'.$base_key . '[status]"]' => array("checked" => TRUE))
      ),
    );
    $ovverides = !empty($template_data_mapping->ovverides) ? unserialize($template_data_mapping->ovverides) : array();
    
    $form[$base_key]['ovverides']['template_name_ovveride'] = array(
      '#type' => 'textfield',
      '#title' => t('Override template name'),
      '#description' => t('Override template names shown on entity add/edit forms. Leave blank to use default.'),
      '#default_value' => !empty($ovverides['template_name_ovveride']) ? $ovverides['template_name_ovveride'] : NULL,
    );
    
    $form[$base_key]['ovverides']['allowed_extensions'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Allowed file types to be downloaded'),
      '#options' => reporting_cloud_return_format_options_list(),
      '#default_value' => !empty($ovverides['allowed_extensions']) ? $ovverides['allowed_extensions'] : reporting_cloud_return_format_options_list(),
    );

    $form[$base_key]['ovverides']['display_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Display title'),
      '#description' => t('Display title of link'),
      '#default_value' => !empty($ovverides['display_title']) ? $ovverides['display_title'] : NULL,
    );
    
    $form[$base_key]['ovverides']['display_title_extension'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include file extension to display title'),
      '#description' => '',
      '#default_value' => !empty($ovverides['display_title_extension']) ? $ovverides['display_title_extension'] : NULL,
    );
    
    
    $status_field_name = $template_data_mapping->name . '[status]'; 
    $form[$base_key]['mapping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mapping'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#states' => array(
        'visible' => array(':input[name="'.$base_key . '[status]"]' => array("checked" => TRUE))
      ),
    );

    $metadata = unserialize($template_data_mapping->metadata);
    $mappings = !empty($template_data_mapping->mapping) ? unserialize($template_data_mapping->mapping) : array();
    
    if (!empty($metadata['merge_blocks'])) {
      $form[$base_key]['mapping']['merge_blocks'] = _reporting_cloud_build_merge_blocks_form($metadata['merge_blocks'], $mappings['merge_blocks']);
    }

    $form[$base_key]['mapping']['field_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Field Mapping'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
   );
   
   foreach ($metadata['merge_fields'] as $field_info) {
    $form[$base_key]['mapping']['field_mapping'][$field_info['name']] = array(
      '#type' => 'textfield',
      '#title' => filter_xss($field_info['text']),
      '#default_value' => (isset($mappings['field_mapping'][$field_info['name']])) ? $mappings['field_mapping'][$field_info['name']] : NULL,
    );
   }
    $available_templates[$template_data_mapping->template_id] = $template_data_mapping->name;
  }
  
  $form_state['templates'] = $available_templates;
  //get all mappings
  
  $form['actions'] = array(
    '#type' => 'actions',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save mappings'),
  );
  
  return $form;
}

/**
* Implements hook_form_submit().
*/
function reportingcloud_entity_edit_form_submit($form, &$form_state) {
  //delete for entity type
  $entity_type = $form_state['entity_type'];
  $bundle = $form_state['bundle'];
  $available_templates = $form_state['templates'];
  $values = $form_state['values'];
  foreach ($available_templates as $template_id => $template_name) {
   $base_key = 'template_' . $template_id;
   db_merge('reporting_cloud_template_mapping')
      ->key(array('entity_type' => $entity_type, 'bundle' => $bundle, 'template_id' => $template_id))
      ->fields(array('entity_type' => $entity_type, 
          'bundle' => $bundle, 
          'template_id' => $template_id,
          'mapping' => serialize($values[$base_key]['mapping']),
          'ovverides' => serialize($values[$base_key]['ovverides']),
          'status' => $values[$base_key]['status'],
        ))
      ->execute();
  }
  
  //delete all templates for this entity_type bundle which is not in available templates
  db_delete('reporting_cloud_template_mapping')
   ->condition('entity_type', $entity_type)
   ->condition('bundle', $bundle)
   ->condition('template_id', array_keys($available_templates), 'NOT IN')
   ->execute();
   
   drupal_set_message(t('Your changes have been saved.'));
}


